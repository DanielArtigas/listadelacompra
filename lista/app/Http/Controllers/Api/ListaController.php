<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Lista;
use App\Elemento;

class ListaController extends Controller
{

    public function index()
    {
        return Lista::with('elementos','user')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules=[
            'nombre' => 'required|min:4|max:15|unique:listas,nombre',
            'user_id' =>' exists:users,id',
        ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $validator=Validator::make($request->all(),$rules,$messages);

        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $lista= new Lista;
        $lista->fill($request->all());
        $lista->save();

        return $lista;
    }
    public function show($id)
    {
        $lista =  Lista::with('elementos')->find($id);
        if($lista){
            return $lista;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'nombre' => 'required|min:4|max:15|unique:listas,nombre',
            'user_id' =>' exists:users,id',
        ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $validator=Validator::make($request->all(),$rules,$messages);

        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $lista=Lista::with('elementos','user')->find($id);

        if(!$lista){
            return response()->json([
                 'message'=>"Lista no actualizada ni encontrada",
            ],404);
        }

        $lista->fill($request->all());
        $lista->save();
        $lista->refresh();

        return $lista;

    }

    public function addElemento(Request $request,$id){
        $rules=[
          'texto' => 'required|min:4|max:15',

         ];

         $messages = [
           'required' => 'Falta por escribir el texto',
           'min'=> 'minimo tiene que tener 4 caracteres',
           'max'=>'no puede tener más de 15 caracteres'
         ];

         $validator=Validator::make($request->all(),$rules,$messages);

         if($validator->fails()) {
           return response()->json($validator->errors(),400);
       }

        $elemento= new Elemento;

        $elemento->texto = $request->texto;
        $elemento->lista_id = $id;
        $elemento->save();

        $lista= Lista::with('elementos')->find($id);
        return $lista;

   }
   
    public function destroy($id)
    {
        Lista::destroy($id);
        return response()->json([
                'message'=>'Deleted',
        ],201);
    }
}
