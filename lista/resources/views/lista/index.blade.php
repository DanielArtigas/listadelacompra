@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <div class="card-header">
        <h1>Listas</h1>
      </div>
      <div class="card-header">Mis listas <br>
           @if(Session::has('lista'))

             Listas Favoritas : {{Session::get('lista')->nombre}}

           @endif

        </div>
        <div class="card-body">
        <a class="btn btn-primary" href="/listas/create" role="button">Nueva</a><br><br>
          <table class="table">
          <tr>
            <th>Nombre</th>
            <th>Usuario</th>
            <th>Acciones</th>
            </tr>
          @forelse($listas as $lista)
          <tr>
            <td>
              {{$lista->nombre}}
            </td>
            <td>
              {{$lista->user->name}}
            </td>
            @can('view', $lista)
            <td>
              <a  href="/listas/{{ $lista->id}}" class="btn btn-primary"  role="button" >Ver</a>
              <a  href="/listas/{{ $lista->id}}/edit" class="btn btn-primary"  role="button" >Editar</a>
              <a  href="/listas/{{$lista->id}}/favorita"><span class="fa fa-star" style="color: orange"></span></a>
            </td>
            @else
            <td>no se puede ver</td>
            @endcan
            <td>
            <form method="post" action="/listas/{{ $lista->id }}">
              {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="Borrar" class="btn btn-danger"  role="button">
            </form>
            </td>
          </tr>
          @empty
            No hay ninguna lista creada
          @endforelse
          </table>
        </div>
    </div>
  </div>
</div>
@endsection