@extends('layouts.app')

@section('title', 'Listas')

@section('content')
  <div class="card-header">
  <h1>Lista: {{ $lista->nombre }}</h1>
  </div>
            <p><strong>Elementos de la lista</strong></p>
            <table class="table">
              @forelse($lista->elementos as $elemento)
              <tr>
                <td>
                {{$elemento->texto}}
                </td>
                <td>
                @if( $elemento->hecho )
                <a href="/elementos/{{ $elemento->id }}/hacer"><span class="fa fa-check" style="color: green"></span></a>
                @else
                <a href="/elementos/{{ $elemento->id }}/deshacer"><span class="fa fa-remove" style="color: red"></span></a>
                @endif
                <a href="/elementos/{{ $elemento->id }}/eliminar"><span class="fa fa-trash" style="color: red"></span></a>
                </td>
              </tr>
              @empty
              Lista vacia
              @endforelse
            </table>

            
  <div class="card-footer">
    <form class="form-inline" method="post" action="/listas/{{$lista->id}}/elementos">
    {{ csrf_field() }}
    <div class="form-group">
    <label for="texto">Texto:</label>
    <input type="texto" class="form-control" id="texto" name="texto">
    </div>
    <button type="submit" class="btn btn-primary">Añadir</button>
    <a href="/listas" class="btn btn-primary"  role="button">Volver</a>
    </form>
  </div>

@endsection
