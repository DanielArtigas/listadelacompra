@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Create de Listas</h1>
            <form method="post" action="/listas">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="nombre">

                    @if ($errors->first('nombre'))
                    <div class="alert alert-danger ">
                        {{$errors->first('nombre')}}
                    </div>
                    @endif
                </div>

                <input type="submit" value="Crear lista" class="btn btn-primary"  role="button">

                <a href="/listas" class="btn btn-primary"  role="button">Volver</a>
            </form>
        </div>

    </div>
</div>
@endsection