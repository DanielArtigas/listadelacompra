<?php

namespace App\Policies;

use App\User;
use App\Lista;
use Illuminate\Auth\Access\HandlesAuthorization;

class ListaPolicy
{
    use HandlesAuthorization;

    public function index(User $user, Lista $lista)
    {
        //
    }

    public function view(User $user, Lista $lista)
    {
        if($user->id == 1){
            return true;
        }
        else
        {
            return $user->id == $lista->user_id;
        }
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Lista $lista)
    {
        if($user->id == 1){
            return true;
        }
        else
        {
            return $user->id == $lista->user_id;
        }
    }

    public function delete(User $user, Lista $lista)
    {
        //
    }


    public function restore(User $user, Lista $lista)
    {
        //
    }

    public function forceDelete(User $user, Lista $lista)
    {
        //
    }

    public function manage(User $user)
    {
        return $user->id == 1;
    }
}
