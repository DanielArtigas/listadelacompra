<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lista;
use App\Elemento;
use App\User;
use Session;
use Auth;

class ListaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // return "listas";
        $listas = Lista::all();
        $user = \Auth::user();
        if($user->can('manage', Lista::class)){
            $listas = Lista::all();
        } else{
            $listas = Lista::where('user_id',$user->id)->get();
        }
        
        return view('lista.index', ['listas' => $listas]);
    }

    public function create()
    {
        $listas = Lista::all();
        return view('lista.create', ['listas' => $listas]);
    }

    public function store(Request $request)
    {
        $rules=[
            'nombre' => 'required|max:15|min:4|unique:listas,nombre',
        ];

        $request->validate($rules);

        $listas = new Lista;
        $listas->fill($request->all());
        $listas->user_id = \Auth::user()->id;
        $listas->save();

        return redirect('/listas');
    }

    public function show($id)
    {
        $lista=Lista::find($id);
        $this->authorize('view', $lista);
        return view('lista.show', ['lista' => $lista]);
    }

    public function edit($id)
    {
        $lista=Lista::find($id);
        $this->authorize('update', $lista);
        return view('lista.edit',['lista'=>$lista]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'nombre' => 'required|max:20|min:4|unique:listas,nombre,' . $id,
        ];

        $request->validate($rules);

        $listas = Lista::findOrFail($id);
        $listas->fill($request->all());
        $listas->user_id = \Auth::user()->id;
        $listas->save();
        return redirect('/listas');
    }

    public function destroy($id)
    {
        Lista::destroy($id);
        return back();
    }

    public function addElemento($id, Request $request)
    {
        $rules=[
            'texto' => 'unique:elementos,texto|required|max:15|min:4',
        ];

        $request->validate($rules);
        $elemento = new Elemento;
        $elemento->texto = $request->texto;
        $elemento->lista_id = $id;
        $elemento->save();
        return back();
    }

    public function hacer($id)
    {
        $elemento = Elemento::find($id);
        $elemento->hecho = false;
        $elemento->save();
        return back();
    }

    public function deshacer($id)
    {
       $elemento = Elemento::find($id);
       $elemento->hecho = true;
       $elemento->save();
       return back();
    }

    public function eliminar($id)
    {
        Elemento::destroy($id);
        return back();
    }

    public function favorita($id,Request $request)
    {
        $lista=Lista::find($id);
        $request->session()->put('lista',$lista);
        return back();
     }
}
